#Crie um programa que possua um método que resolva a potência dado um número base e seu expoente. Estes dois valores devem ser informados pelo usuário

def calcula(base,expoente)
      resultado = base**expoente
      puts "O número #{base} elevado a #{expoente} é #{resultado}"  
end

print "Digite o primeiro numero: "
base = gets.chomp.to_i
print "Digite o segundo numero: "
expoente = gets.chomp.to_i

calcula(base,expoente)
