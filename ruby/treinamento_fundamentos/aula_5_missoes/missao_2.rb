#Siga a documentação da gem cpf_cnpj para criar um programa que recebe como entrada um número de cpf e em um método verifique se este número é válido.


require "cpf_cnpj"
 
def validaCpf(numero_cpf)
 if CPF.valid?(numero_cpf)
   return "O cpf informado é valido"
 else
   return "O cpf informado é invalido"
 end
end
 
print 'Digite seu cpf: '
numero_cpf = gets.chomp
 
resulto = validaCpf(numero_cpf)
 
puts resulto