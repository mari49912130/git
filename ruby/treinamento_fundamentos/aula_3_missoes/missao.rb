loop do
    puts 'Selecione uma das seguintes opções:'
    puts '[1] Somar'
    puts '[2] Diminuir'
    puts '[3] Multiplicar'
    puts '[4] Dividir'
    puts '[5] Potência'
    puts '[0] Sair'
    print 'Opção: '
    
    option = gets.chomp.to_i
    messagesResultado = 'Resultado: '
    sleep 0.1
    system "cls"

    case option 
    when 1
      puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_f
        numero_2  = gets.chomp.to_f
        resultado = numero_1 + numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    when 2 
      puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_f
        numero_2  = gets.chomp.to_f
        resultado = numero_1 - numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    when 3 
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_f
        numero_2  = gets.chomp.to_f
        resultado = numero_1 * numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    when 4
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_f
        numero_2  = gets.chomp.to_f
        resultado = numero_1 / numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    when 5 
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_f
        numero_2  = gets.chomp.to_f
        resultado = numero_1 ** numero_2 
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    when 0 
        puts 'Saindo...'
        sleep 0.8
        system 'cls'
        break 
    else 
      puts 'Opção não encontrada' 
      sleep 1
      system 'cls'
     end 
end