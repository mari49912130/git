#Crie uma collection do tipo Hash e permita que o usuário crie três elementos informando a chave e o valor. No final do programa para cada um desses elementos imprima a frase “Uma das chaves é **** e o seu valor é ****”

hash = Hash.new



i = 1
 
1..3.times do 
      sleep 1
      system 'cls'
      print "Digite a #{i}ª chave... "
      chave = gets.chomp
      print "Digite o #{i}º valor..."  
      valor = gets.chomp             
      hash[chave] = valor
      
      i += 1
end

sleep 1.2
system 'cls'      
puts " #{hash}"
puts ''

hash.each do |key, value|
      puts " Uma das chaves é #{key} e o seu valor é #{value}"
      puts ''
end

