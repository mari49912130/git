#Dado o seguinte hash:
#Numbers = {a: 10, b: 30 2, c: 20, d: 25, e: 15}
#Crie uma instrução que seleciona o maior valor deste hash e no final imprima a chave e valor do elemento resultante.

numbers = {a: 10, b: 30, c: 20, d: 25, e: 15}

maiorValor = 0
result  = Hash.new

numbers.each do |key, value|
  if value > maiorValor
    maiorValor = value
    result = [key, value]
  end
end
puts "Maior chave e valor: #{result}"

