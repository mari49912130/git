#Adicionando itens
#estados.push('Espírito Santo')
#estados.push('Minas Gerais', 'Rio de Janeiro', 'São Paulo')

#Exiba o array estados
#puts estados

#Comando  insert(especifica a posição que os elementos ocuparão)
#estados.insert(0, 'Acre', 'Amapá')

#Acessando elementos
#estados[1]
#estados[-2]
#estados.first
#estados.last

#Acessando intervalos de elementos 
#estados[2..5]

#Quantidade de itens em um Array
#estados.count
#estados.length

#Descubra se o array está vazio
#estados.empty?

#Verifique se um item específico está presente
#estados.include?('São Paulo')

#Remova um item através de seu índice
#estados.delete_at(2)

#Exclua o ultimo item do array estados
#estados.pop

#Para excluir o primeiro item faça
#estados.shift