loop do
    puts 'Selecione uma das seguintes opções:'
    puts '[1] Somar'
    puts '[2] Diminuir'
    puts '[3] Multiplicar'
    puts '[4] Dividir'
    puts '[5] Potência'
    puts '[0] Sair'
    print 'Opção: '
    
    option = gets.chomp.to_i
    messagesResultado = 'Resultado: '
    sleep 0.1
    system "cls"

    if option == 1  
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_i
        numero_2  = gets.chomp.to_i
        resultado = numero_1 + numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
        
    elsif option == 2 
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_i
        numero_2  = gets.chomp.to_i
        resultado = numero_1 - numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    elsif option == 3 
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_i
        numero_2  = gets.chomp.to_i
        resultado = numero_1 * numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    elsif option == 4
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_i
        numero_2  = gets.chomp.to_i
        resultado = numero_1 / numero_2
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    elsif option == 5 
        puts 'Aguardando valores...'  
        numero_1 = gets.chomp.to_i
        numero_2  = gets.chomp.to_i
        resultado = numero_1 ** numero_2 
        puts messagesResultado + "#{resultado}"
        sleep 1.5
        system 'cls'
    elsif option == 0 
        puts 'Saindo...'
        sleep 0.8
        system 'cls'
        break 
    else
        puts 'Opção não encontrada' 
    end    
end