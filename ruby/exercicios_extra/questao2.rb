#Crie um script em Ruby que leia três valores, 'a', 'b' e 'c' e diga se estes valores podem ser os lados de um triângulo. Para um triângulo ser formado, a soma de dois lados deve ser maior do que o terceiro lado: a + b > c e a + c > b e b + c > a.

puts'Informe l1:'
l1 = gets.chomp.to_i

puts'Informe l2:'
l2 = gets.chomp.to_i

puts'Informe l3:'
l3 = gets.chomp.to_i


if l1 + l2 > l3 && l2 + l3 > l1 && l1 + l3 > l2
   puts "#{l1}, #{l2} e #{l3} formam um triãngulo"

   if l1 == l2 && l2 == l3 && l1 == l3
      puts 'Este é um triângulo equilátero'
   elsif l1 == l2 && l1 != l3
      puts'Este é um triangulo isósceles '
   else
      puts'Este é um triangulo escaleno'
   end
else
   puts'os lados informados não foram um triangulo'
end