
-> git config --global user.name "nome_do_usuario"	                     - Faz a configuracao do nome de usuario

-> git config --global user.email "email_do_usuario"                     - Faz a configuracao do e-mail do usuario

-> git init                                                              - Inicializa repositorio Git

-> git clone ssh://git@github.com/<usuario>/<nome-repositorio>.git       - Adicionar um repositorio remoto

-> git add .                                                             - Adiciona todos os arquivos para area de preparacao

-> git add <nome_do_arquivo.extensao>                                    - Adiciona unico arquivo para area de preparacao

-> git rm -r <nome-arquivo.txt>                                          - Remove arquivo ou pasta

-> git commit -m "mensagem_commit"                                       - Salva mudancas no repositorio local e adiciona mensagem de commit

-> git status                                                            - Verifica estados dos arquivos do repositorio

-> git log                                                               - Mostra historico de alteracoes em ordem cronologica

-> git diff                                                              - Mostra alteracoes na staged area

-> git tag                                                               - Faz a listagem de tags

-> git tag <nome_da_tag> -m "mensagem"                                   - Faz a criacao de uma tag no ultimo commit da branch  
                                                                           corrente juntamente com uma mensagem
                                                                    
-> git tag -d <nome_da_tag>                                              - Faz a remocao da tag localmente

-> git branch                                                            - Faz a listagem de todas as branches locais

-> git branch -d <branch_name>                                           - Faz a remocao de um branch local

-> git branch -m <nome_antigo> <nome_novo>	                             - Renomeia uma branch especifica localmente que nao e a corrente

-> git checkout -	                                                     - Muda para a ultima branch

-> git checkout <branch_name>	                                         - Muda para outra branch

-> git checkout -b <branch_name>	                                     - Cria uma nova branch e ja faz checkout para a mesma

-> git pull origin <branch_name>	                                     - Recebe alteracoes do repositorio remoto

-> git push origin --delete <nome da branch>	                         - Faz a remocao de uma branch remota

-> git reset --hard	                                                     - Desfaz todas as alteracoes que aconteceram em arquivos rastreados

-> git merge <nome_da_branch>	                                         - Mescla as mudancas presentes na <nome_da_branch> na branch corrente



